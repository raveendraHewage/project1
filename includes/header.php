<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="css/main.css"> 
	<style>
		h2	{
			background: black;
			border-radius:20px;
			padding: 2px 2px 2px 20px;
			color:white;
			font-family: Comic Sans MS;
			border:2px solid black;
		}
		ul.header {
			font-family: Comic Sans MS;
			padding: 10px 10px 10px 10px;
			display: inline-flex;
			width: 98.65%;
			background: black;
		}
		form {
			border:2px solid gray;
			background: white;
		}
		a:link {
		  color: black;
		  text-decoration: none;
		}

		a:visited {
		  color: black;
		  text-decoration: none;
		}

		a:hover {
		  color: gray;
		}

		a:active {
		  color: gray;
		}

input[type=text], [type=email],[type=password] {
  width: 20%;
  padding: 3px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: black;
  color: white;
  padding: 3px 10px 3px 10px;
  border: 1px solid black;
  border-radius: 4px;
  cursor: pointer;
  float: center;
}
input[type=submit]:hover {
  background-color: gray;
}
input[class=search] {
  background-color: black;
  color: white;
  padding: 4px 20px;
  border: 1px solid black;
  border-radius: 4px;
  cursor: pointer;
  float: center;
}
input[class=search]:hover {
  background-color: gray;
}
input[class=bar] {
  width: 40%;
  padding: 3px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}
nav.header {
	width:97%;
	height: auto;
	float: center;
}
div[class=option]:hover{
	background: white;
}
@media screen and (min-width: 601px) {
div.go {
	width: 10%;
	height: auto;
	border: 2px solid black;
	border-radius: 10px;
	padding: 10px 0px 10px 0px;
	background: gray;
}
}

@media screen and (max-width: 600px){
div.go {
	width: 10%;
	height: auto;
	border: 2px solid black;
	border-radius: 10px;
	padding: 10px 0px 10px 0px;
	background: gray;
}
}
div[class=go]:hover{
	background-color: white;
}

	</style>
</head>
<body>
	<header>
		<h1 style="font-weight: bold;">
			නේවාසිකයෝ
		</h1>
	</header>
	<nav>
		<ul class="header">
			<div class="go">
				<a href="logIn.php?stuNumber=<?php echo $_GET['stuNumber']; ?>"><b>LOG OUT</b></a>
			</div>
			<div class="go">
				<a href="home.php?stuNumber=<?php echo $_GET['stuNumber']; ?>"><b>HOME</b></a>
			</div>
			<div class="go">
				<a href="profileInfo.php?stuNumber=<?php echo $_GET['stuNumber']; ?>"><b>PROFILE</b></a>
			</div>
			<div class="go">
				<a href="boys.php?stuNumber=<?php echo $_GET['stuNumber']; ?>"><b>BOADERS</b></a>
			</div>
			<div class="go">
				<a href="memories.php?stuNumber=<?php echo $_GET['stuNumber']; ?>"><b>MEMORIES</b></a>
			</div>
			<div class="go">
				<a href="displayPosts.php?stuNumber=<?php echo $_GET['stuNumber']; ?>"><b>POSTS</b></a>
			</div>
			<div class="go">
				<a href="createPost.php?stuNumber=<?php echo $_GET['stuNumber']; ?>"><b>NEW POSTS</b></a>
			</div>
		</ul>
	</nav>