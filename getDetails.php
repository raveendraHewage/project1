<?php include_once('includes/connection.php'); ?>
<?php require('includes/header.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<style>
h1 {
  text-align: center;
}

p.date {
  text-align: right;
}

p.main {
  text-align: justify;
}
body {
	background-image: url("images/background.png");
	background-repeat: repeat;
	background-size: contain;
	background-attachment: fixed;
}
form.change {
	background-color:rgba(255, 255, 255, 0.3);
	border:2px solid gray;
	font-family: Comic Sans MS;
	font-size: 20px;
}
input[type=text], [type=email],[type=password] {
  width: 20%;
  padding: 3px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: black;
  color: white;
  padding: 4px 20px;
  border: 1px solid black;
  border-radius: 4px;
  cursor: pointer;
  float: center;
}
input[type=submit]:hover {
  background-color: gray;
}
img.proPicture {
	border-radius: 50%;
}
</style>
	<title>Edit Details</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<h2><?php echo "Edit Details Of ".$_GET['name']; ?></h2>
	<center>
	<article>
		<?php 
		echo '<form action="profileInfo.php?stuNumber='.$_GET['stuNumber'].'" method="post" enctype="multipart/form-data" class="change">';
			if(!empty($_GET['photoName']))
			{
				echo '<img src="images/boaders/'.$_GET['photoName'].'" class="proPicture" width="200" height="200" alt="photo" style="border:2px solid gray;">';
			}
			else
			{
				echo '<img src="images/unknown.png" class="proPicture" height="200" title="Logo of a company" alt="photo" style="border:2px solid gray;">';
			} ?>
			<br>
		<b>Student Number</b><br><input type="text" name="stuNumber" value="<?php echo $_GET['stuNumber']; ?>" id=""><br>
		<b>Index Number</b><br><input type="text" name="indNumber" value="<?php echo $_GET['indNumber']; ?>" id=""><br>
		<b>First Name</b><br><input type="text" name="firstName" value="<?php echo $_GET['firstName']; ?>" id=""><br>
		<b>Last Name</b><br><input type="text" name="lastName" value="<?php echo $_GET['lastName']; ?>" id=""><br>
		<b>E-mail</b><br><input type="email" name="email" value="<?php echo $_GET['email']; ?>"id=""><br>
		<b>Phone Number</b><br><input type="text" name="phoNumber" value="<?php echo $_GET['phoNumber']; ?>"id=""><br>
		<p><b>Upload a new photo</b></p>
		<input type="file" name="image" id=""><br>
		<input type="submit" value=" Save " onclick="return confirm('Are you sure?')" name="submit">
	 </form>
	</article>
	</center>
</body>
</html>
<?php mysqli_close($connection); ?>
<?php include_once('includes/footer.php'); ?>