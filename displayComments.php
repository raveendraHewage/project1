<?php include_once('includes/connection.php'); ?>
<?php require('includes/header.php'); ?>
<?php 
	if(isset($_POST['submit']))
	{
		$comment=$_POST['newComment'];
		$comPhoto=$_FILES['comPhoto']['name'];
		$tmpName=$_FILES['comPhoto']['tmp_name'];
		$stuNumber=$_GET['stuNumber'];
		$postId=$_GET['postId'];
		$uploadTo="images/comPhotos/";

		$query="INSERT INTO comments (postId,stuNumber,comment,comPhoto)VALUES('{$postId}','{$stuNumber}','{$comment}','{$comPhoto}')";
		$result=mysqli_query($connection,$query);
		if(!empty($comPhoto))
		{
			move_uploaded_file($tmpName, $uploadTo.$comPhoto);
		}
	}

 ?>
<!DOCTYPE html>
<html>
<head>
	<style>
h1 {
  text-align: center;
}

p.date {
  text-align: right;
}

p.main {
  text-align: justify;
}
body {
	background-image: url("images/background.png");
	background-repeat: repeat;
	background-size: contain;
	background-attachment: fixed;
}
div.posts {
	background-color:rgba(255, 255, 255, 0.3);
	border:2px solid gray;
	padding: 10px 10px 10px 10px;
	font-size: 20px;
	font-family: Comic Sans MS;
}
div.postImg,.frame { 
	background-color:rgba(255, 255, 255, 0.3);
	border:2px solid gray;
	padding: 10px 10px 10px 10px;
	font-size: 20px;
	font-family: Comic Sans MS;
}
form.newCom {
	background-color:rgba(255, 255, 255, 0.2);
	border:2px solid gray;
}
img.comImg {
	width:25%;
	max-height:auto;
}

</style>

	<title>Comments</title>
	<link rel="stylesheet" href="css/main.css"> 
</head>
<body>
	<h2>Comments</h2>
	<article>
	<?php 

	echo '<div class="postImg" ><center>';
	if(!empty($_GET['mediaName']))
	{
		echo '<img src="images/postMedia/'.$_GET['mediaName'].'" title="photo" width="150" height="150" alt="photo" style="border:2px solid gray;"/>';
	}
	$query2="SELECT postText FROM posts WHERE postId={$_GET['postId']}";
	$postInfo=mysqli_query($connection,$query2);
	$record2=mysqli_fetch_assoc($postInfo);
	echo '<br>';
	echo $record2['postText'];
	

	echo '<center><form action="displayComments.php?stuNumber='.$_GET['stuNumber'].'&postId='.$_GET['postId'].'&mediaName='.$_GET['mediaName'].'&name='.$_GET['name'].'" method="post" class="newCom" enctype="multipart/form-data" class="post">
			<p><b>Upload a photo</b></p>
		<input type="file" name="comPhoto" id=""><br>
		<textarea rows = "5" cols = "80" name = "newComment">
         </textarea><br>
		<input type="submit" value=" Add Comment " name="submit">
	 </form></center>';
	 echo '</div></center>';

	$query="SELECT stuNumber,comment,comPhoto,postId,commentId FROM comments WHERE postId={$_GET['postId']} ORDER BY commentId DESC;";
	$usersInfo=mysqli_query($connection,$query);

	if($usersInfo)
	{
		$i=0;
		$records=mysqli_num_rows($usersInfo);
		while($i< $records)
		{
			$record=mysqli_fetch_assoc($usersInfo);
			$query1="SELECT firstName,lastName,photoName FROM users WHERE stuNumber='{$record['stuNumber']}'";
			$usersInfo1=mysqli_query($connection,$query1);
			$record1=mysqli_fetch_assoc($usersInfo1);
			$name=$record1['firstName']." ".$record1['lastName'];
			echo '<div class="frame">';
		
			if($record['stuNumber']==$_GET['stuNumber'])
			{
				if(!empty($record1['photoName']))
				{
					echo '<a href="profileInfo.php?stuNumber='.$_GET['stuNumber'].'"><img src="images/boaders/'.$record1['photoName'].'" height="40" width="40" title="photo" alt="photo" style="border:2px solid gray;border-radius:50%;"></a>';
				}
				else
				{
					echo '<a href="profileInfo.php?studentNumber=stuNumber='.$_GET['stuNumber'].'"><img src="images/unknown.png" height="auto" width="40" title="40" alt="photo" style="border:2px solid gray;border-radius:50%;"></a>';
				}

				echo '<a href="profileInfo.php?stuNumber='.$_GET['stuNumber'].'"><b>'.$name.'</b></a>';
			}
			else
			{
				if(!empty($record1['photoName']))
				{
					echo '<a href="othersProfile.php?studentNumber='.$record['stuNumber'].'&stuNumber='.$_GET['stuNumber'].'"><img src="images/boaders/'.$record1['photoName'].'" height="40" width="40" title="photo" alt="photo" style="border:2px solid gray;border-radius:50%;"></a>';
				}
				else
				{
					echo '<a href="othersProfile.php?studentNumber='.$record['stuNumber'].'&name='.$record['name'].'&stuNumber='.$_GET['stuNumber'].'"><img src="images/unknown.png" height="auto" width="40" title="40" alt="photo" style="border:2px solid gray;border-radius:50%;"></a>';
				}

				echo '<a href="othersProfile.php?studentNumber='.$record['stuNumber'].'&stuNumber='.$_GET['stuNumber'].'"><b>'.$name.'</b></a>';
			}
			echo '<br><hr>';
			echo '<center>';
			if(!empty($record['comPhoto']))
			{
				echo '<img src="images/comPhotos/'.$record['comPhoto'].'" height="300" class="comImg" alt="photo" style="border:2px solid gray;"/>';
			}
			echo '<br>';
			if(!empty($record['comment']))
			{
				echo $record['comment'];
			}
			echo '<hr>';

			$query3="SELECT reply FROM replies WHERE commentId={$record['commentId']}";
			$result3=mysqli_query($connection,$query3);
			$record3=mysqli_num_rows($result3);
			if($record3==0)
			{
				echo "<i>No replies yet.</i>";
			}
			else if($record3==1)
			{
				echo '<a href="displayReplies.php?stuNumber='.$_GET['stuNumber'].'&postId='.$_GET['postId'].'&comPhoto='.$record['comPhoto'].'&name='.$_GET['name'].'&commentId='.$record['commentId'].'&id=0&my='.$_GET['my'].'"><i>'.$record3.' reply</i></b></a>';
			}
            else if($record3>1)
            {
                echo '<a href="displayReplies.php?stuNumber='.$_GET['stuNumber'].'&postId='.$_GET['postId'].'&comPhoto='.$record['comPhoto'].'&name='.$_GET['name'].'&commentId='.$record['commentId'].'&id=0&my='.$_GET['my'].'><i>'.$record3.' replies</i>"</b></a>';
            }
            echo '<br>';
			echo '<a href="displayReplies.php?stuNumber='.$_GET['stuNumber'].'&postId='.$_GET['postId'].'&comPhoto='.$record['comPhoto'].'&name='.$_GET['name'].'&commentId='.$record['commentId'].'&id=0&my='.$_GET['my'].'"><input type="submit" value=" Reply " name="submit"></b></a>';

			if($_GET['my']==1)
			{
				echo '<a href="deleteComment.php?stuNumber='.$_GET['stuNumber'].'&postId='.$_GET['postId'].'&mediaName='.$_GET['mediaName'].'&name='.$_GET['name'].'&comPhoto='.$record['comPhoto'].'&commentId='.$record['commentId'].'&id=0&my='.$_GET['my'].'"><input type="submit" value=" Delete Comment " name="submit"></b></a>';
			}
			else
			{
				if($record['stuNumber']==$_GET['stuNumber'])
				{
					echo '<a href="editComment.php?stuNumber='.$_GET['stuNumber'].'&postId='.$record['postId'].'&commentId='.$record['commentId'].'&mediaName='.$_GET['mediaName'].'&name='.$_GET['name'].'&id=0my='.$_GET['my'].'"><input type="submit" value=" Edit Comment " name="submit"></b></a>';
					echo '<a href="deleteComment.php?stuNumber='.$_GET['stuNumber'].'&postId='.$_GET['postId'].'&mediaName='.$_GET['mediaName'].'&name='.$_GET['name'].'&comPhoto='.$record['comPhoto'].'&commentId='.$record['commentId'].'&id=0&my='.$_GET['my'].'"><input type="submit" value=" Delete Comment " name="submit"></b></a>';
				}
			}
			echo '</div>';
			echo '</center>';
			$i++;
			if($i<$records)
			{
				echo '<hr>';
			}
		}
	}
	else
	{
	 	echo "No comments yet.";
	}
	echo '</div>';
	 ?>
	</article>
</body>
</html>
<?php mysqli_close($connection); ?>
<?php include_once('includes/footer.php'); ?>